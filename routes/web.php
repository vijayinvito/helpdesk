<?php

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::get('register', function () {
    abort(404);
});  

Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth', 'Role:10']], function () {
    Route::get('/', 'DashboardController@index')->name('dash');
    Route::resource('users', 'UserController');
    Route::get('/verify', 'UserController@VerifyUser')->name('verify');
    Route::get('/unverify', 'UserController@UnverifyUser')->name('unverify');;
    Route::get('userdeactivate/{id}', 'UserController@deactivate')->name('userdeativete');
    Route::get('useractivate/{id}', 'UserController@activate')->name('useractivate');;


   

});

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', function () {
    return view('welcome');
});
