<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::apiResource('/user', 'ApisController'); 


   //  API Routes 

 Route::post('/storeimei','MieiController@CreateAccount');

 Route::post('/signup','UserController@SignUp');

 Route::post('/signin','UserController@SignIn');

 Route::post('/forgot','UserController@ForgotPassword');
 
 Route::post('/verifyemail','UserController@VerifyEmail');
 
 Route::post('/logout','UserController@LogOut');
 
 Route::post('/setnewpassword','UserController@SetNewPassword');
