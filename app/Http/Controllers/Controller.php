<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct() {
		date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    
    static public function Response($status = false,$message = "Something went Wrong",$payload = NULL,$code = 404){

      return response()->json(
          ['code'=> $code,
          'status' => $status,
          'message' => $message,
          'payload'=> $payload
          ],$code);
  }
    
     function test($one,$two)
    {
        return $one + $two ;
    }

}
