<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\Miei;

class MieiController extends Controller{
    
    public function CreateAccount(Request $request){
        $validator =Validator::make($request->all(),[
            "imei_number"     =>  'required ',
            ]);
            if($validator->fails()){
            $message = $validator->errors()->first();
            $status = false;
            $code = 400;
            $payload = [];
            
            }else{
                $imei = Miei::where('imeinumber',$request->imei_number);
                if($imei->exists()){
                    
                    $imei->where('user_id','=',NULL);
                    if($imei->exists()){
                        $status = true;
                        $code = 201;
                        $message ="Your data is Recover";
                    }else{
                        $status = true;
                        $code = 401;
                        $message ="You have Already Register | Please Signin with Registered Email";
        
                    }
                    $payload = [];
                }else{
                    $payload = Miei::create(['imeinumber'=> $request->imei_number]);
                    $status = true;
                    $code = 200;
                    $message ="Account Created Successfully";
                }
            }
        return $this->response($status,$message,$payload,$code);  
    }

    static public function SyncImei($user_id,$imei){
        Miei::where('imeinumber',$imei)->update(['user_id'=> $user_id]);
    }

    
}
