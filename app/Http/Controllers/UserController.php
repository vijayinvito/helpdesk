<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\Miei;
use App\Model\Otp;
use Mail;
use App\Http\Controllers\MieiController;

class UserController extends Controller
{
    
    public function index(){
        $items = User::VerifyUser();

        return view('admin.users.index', compact('items'));
    }
    public function VerifyUser(){
        $items = User::VerifyUser();

        return view('admin.users.index', compact('items'));
    }
    public function UnverifyUser(){
        $items = User::UnverifyUser();

        return view('admin.users.index', compact('items'));
    }

    public function create(){
        return view('admin.users.create');
    }

    public function store(Request $request){
        $this->validate($request, User::rules());
        User::create($request->all());
        return back()->withSuccess(trans('app.success_store'));
    }

   
    public function show($id){
       $users = User::find(2);
        return $users;
    }

   
    public function edit($id){
        $item = User::findOrFail($id);

        return view('admin.users.edit', compact('item'));
    }


    public function update(Request $request, $id){

       $this->validate($request, User::rules(true, $id,$request));

        $item = User::findOrFail($id);

        $item->update($request->all());

        // return view('admin.employees.index');
        // return redirect()->route(ADMIN . '.users.index')->withSuccess(trans('app.success_update'));
    }


    public function deactivate($id){
     
        $data =  User::where('id','=',$id)
                          
                    ->update(['isdelete'=> 1]);
      
         return back()->withSuccess("User Successfully Deactivated");
    }

     public function activate($id){
      
        $data =  User::where('id','=',$id)
                     
                    ->update(['isdelete'=> 0]);
      
         return back()->withSuccess('User Successfully Activated');
    }

    /** Resgister New User and Store there IMEI numbers if its new IMEI Number  */

    public function SignIn(Request $req){
        $validator =Validator::make($req->all(),[
                            "email"       =>  'required | email',
                            "password"    =>  'required',
                            'imei_number' => 'required|exists:App\Model\Miei,imeinumber',
                            "device_type"    =>  'required',
                            "device_id"    =>  'required',
                            "device_token"    =>  'required',
                            ]);
       if($validator->fails()){
           $message = $validator->errors()->first();
           $status = false;
           $code = 400;
           $payload = [];
          
       }else{
           $user = User::where('email',$req->email);
           if($user->first()){
                $user_data = $user->first();
                if(Hash::check($req->password, $user_data['password'])){
                   
                   $user->update([
                           'device_token' => $req->device_token,
                           'device_id' =>  $req->device_id,
                           'device_type' =>  $req->device_type,
                           'login_status' => 1
                           ]);

                    $imei = Miei::where('imeinumber',$req->imei_number)->where('user_id','=',NULL);
                    $user_data = $user->first();
                    if($imei->exists()){
                        MieiController::SyncImei($user_data->id,$req->imei_number);
                    }
                    $message = "Sign In Successfully";
                    $status = true;
                    $code = 200;
                    $payload = $user_data;
                   
               }else
               {
                   $message = "Password Not Match";
                   $status = false;
                   $code = 404;
                   $payload = [];
               }
           }else{
           $message = "User Not Exist";
           $status = false;
           $code = 404;
           $payload = [];
           }
       }
       return $this->Response($status,$message,$payload,$code);
    }

    /** Resgister New User and Store there IMEI numbers  */

    public function SignUp(Request $req){

        $validator =Validator::make($req->all(),[
                            "email"       =>  'required |unique:users|email',
                            "name"       =>  'required',
                            "imei_number"       =>  'required|exists:App\Model\Miei,imeinumber',
                            "password"       =>  'required',
                            "device_type"       =>  'required',
                            "device_id"    =>  'required',
                            "device_token"    =>  'required',
                            ]);
       if($validator->fails()){
           $message = $validator->errors()->first();
           $status = false;
           $code = 400;
           $payload = [];
       }else{
          
          $user = User::create([
               'email'=> $req->email,
               'name'=> $req->name,
               'password' => $req->password,
               'role'=>'1',
               'device_type' => $req->device_type,
               'device_token' => $req->device_token,
               'device_id' =>  $req->device_id,
               ]);
           $otp = rand(10000,99999);
           Otp::create(['user_id' => $user->id ,'otp' => $otp]);
           $this->sendEmail($user->name,$user->email,$otp);
           MieiController::SyncImei($user->id,$req->imei_number);
           $user->otp =  $otp;
           $message = "SignUp In Successully";
           $status = true;
           $code = 200;
           $payload = $user;
       }
       return $this->Response($status,$message,$payload,$code);
    }

    /** Send OTP On register Email  */
    
    public function ForgotPassword(Request $req){
        $validator =Validator::make($req->all(),[
                            "email"       =>  'required|email',
                            ]);
       if($validator->fails()){
           $message = $validator->errors()->first();
           $status = false;
           $code = 400;
           $payload = NULL;
       }else{
           $user = User::where('email',$req->email)->first();
           if($user){
             
                   $otp = rand(10000,99999);
                   Otp::create(['user_id' => $user->id ,'otp' => $otp]);
                   $this->sendEmail($user->name,$user->email,$otp);
                   $message =" Otp send Successfully";
                   $status =  true;
                   $code = 200;
                   $payload = $otp;
           }else{
           $message = "User Not Exist";
           $status = false;
           $code = 404;
           $payload = NULL;
           }
       }
        return $this->Response($status,$message,$payload,$code);
    } 
    
    /** Send OTP On register Email  */
    
    public function VerifyEmail(Request $req){
        $validator =Validator::make($req->all(),[
                            "email"       =>  'required|email',
                            ]);
       if($validator->fails()){
           $message = $validator->errors()->first();
           $status = false;
           $code = 400;
           $payload = NULL;
       }else{
           $user = User::where('email',$req->email)->first();
           if($user){
             
                  
                   $user = User::where('email',$req->email)->update(['isemailverified'=>1,'email_verified_at'=>date('Y-m-d H:i:s')]);
                   $message = " Email Verify Successfully";
                   $status =  true;
                   $code = 200;
                   $payload = $user;
           }else{
           $message = "User Not Exist";
           $status = false;
           $code = 404;
           $payload = NULL;
           }
       }
        return $this->Response($status,$message,$payload,$code);
    }


   /** Set New Password  */

    public function SetNewPassword(Request $request) {

            $validator =Validator::make($request->all(),[
                        "email"      =>  'required|exists:App\User,email',
                        "password"     =>  'required',  
                        "imei_number"  =>  'required',  
                     ]);

         if ($validator->fails()){

            $message = $validator->errors()->first();
            $status = false;
            $code = 404;
            $payload =[];

            }
        else{     

           $user = User::where('email',$request->email);
           $user->update(['password' => bcrypt($request->password)]);            
           $status = true;
           $code = 200;
           $message = "Your Password Successfully Changed";
           $payload = $user->first();
       }
       return $this->Response($status,$message,$payload,$code);   
    }  


    /**  Send Email Using SMTP and User email.blade template  */ 

    public function sendEmail($name,$email,$otp){
        $this->name = $name ?? 'Internam';
        $this->otp = $otp ?? '123456987789954';
        $this->email = $email ?? 'redmi@gmail.com';
        $this->header = 'Hi,' .$this->name;
        $data['name'] = $this->name;
        $data['otp'] = $this->otp;
        $this->subject = 'Your OTP  is :' .$this->otp;
        Mail::send('email', $data, function($message) {

        $message->to($this->email, $this->header)

                ->subject($this->subject);
        });

        if (Mail::failures()) {
            return response()->json(['Sorry! Please try again latter' => 1]);
        }else{
         return response()->json(['Great! Successfully send in your mail' => 1,'Password' => $this->otp ]);
        }
    }


    /************   Logout User   ******** */

   public function LogOut(Request $req){
       
        $validator =Validator::make($req->all(),[
                           "user_id"      =>  'required',
                           "imei_number"  =>  'required',
                           ]);
        if($validator->fails()){

            $message = $validator->errors()->first();
            $status = false;
            $code = 400;
        
        }else{

            $user = User::where('id',$req->user_id);

            if($user->exists()){
                $user->update([
                      'device_id' => '',
                      'device_token'=> '',
                      'device_type'=> '',
                      'login_status' => 0,
                      ]);
            
                $message = "Logout  Successfully";
                $status =  true;
                $code = 200;
                
            }else{

                $message = "User not found";
                $status = false;
                $code = 404;
            }
        }
        return $this->Response($status,$message,NULL,$code);
    }













     public function api_login(Request $request)
    {
        return User::login($request);
    }


}

