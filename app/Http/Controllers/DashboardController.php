<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Attendance;

class DashboardController extends Controller
{
    public function index()
    {
        $dashinfo =   array(
           'onlineusers' => User::onlineuser()->count(),
           'offlineusers' => User::offlineuser()->count(),
           'activeusers' => User::activeuser()->count(),
           'deleteusers' => User::deleteuser()->count(),
           'verifyuser' => User::VerifyUser()->count(),
           'unverifyuser' => User::UnverifyUser()->count(),
           
           );
            
        return view('admin.dashboard.index',compact('dashinfo'));
    }



 
}
