@php
    $r = \Route::current()->getAction();
    $route = (isset($r['as'])) ? $r['as'] : '';
@endphp

<li class="nav-item mT-30">
    <a class="sidebar-link {{ Str::startsWith($route, ADMIN . '.dash') ? 'active' : '' }}" href="{{ route(ADMIN . '.dash') }}">
        <span class="icon-holder">
            <i class="home icon"></i>
        </span>
        <span class="title">Dashboard</span>
    </a>
</li>

<li class="nav-item">
    <a class="sidebar-link" href="{{ url('/admin/verify') }}">
        <span class="icon-holder">
            <i class="user green icon"></i>
        </span>
        <span class="title"> Verify Users</span>
    </a>
</li>

<li class="nav-item">
    <a class="sidebar-link" href="{{ url('/admin/unverify') }}">
        <span class="icon-holder">
            <i class="user red icon"></i>
        </span>
        <span class="title">Not-Verify Users</span>
    </a>
</li>

