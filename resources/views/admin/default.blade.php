<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="{{asset('public/images/favicon.png')}}" type="image/x-icon">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Voltez | Eng | Admin  Panel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet"> 
    <!-- Semantic- Ui Css -->
    <link href="{{ asset('public/css/semantic/semantic.min.css') }}" rel="stylesheet"> 
    <script src="{{ asset('public/js/jquery-3.4.0.min.js') }}" > </script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/icon.css" rel="stylesheet"> 

    <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/input.css" rel="stylesheet"> 

    <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">

   


	
	@yield('css')

</head>

<body style ="size:90%" class="app">

    @include('admin.partials.spinner')

    <div>
        <!-- #Left Sidebar ==================== -->
        @include('admin.partials.sidebar')

        <!-- #Main ============================ -->
        <div class="page-container">
            <!-- ### $Topbar ### -->
            @include('admin.partials.topbar')

            <!-- ### $App Screen Content ### -->
            <main class='main-content bgc-grey-100'>
                <div id='mainContent'>
                    <div class="container-fluid">

                        <h4 class="c-grey-900 mT-10 mB-30">@yield('page-header')</h4>

						@include('admin.partials.messages') 
						@yield('content')

                    </div>
                </div>
            </main>

            <!-- ### $App Screen Footer ### -->
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>Copyright © 2017 Designed by
                    <a href="http://admin.voltz-eng.com/" target='_blank' title="Voltx - Eng"> ADmin |Voltx - Eng</a>. All rights reserved.</span>
            </footer>
        </div>
    </div>

    <script src="{{ asset('public/js/app.js') }}"></script>
     <!-- Semantic- Ui js -->
    <script src="{{ asset('public/js/semantic.min.js') }}"></script> 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/form.min.js"></script>




    @yield('js')







</body>

</html>
